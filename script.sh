#!/bin/bash

############### FUNCTIONS ################

function floor() {
    echo "$1" | cut -f1 -d"."
}

function ceil() {
    v=$(echo "$1" | cut -f1 -d".")
    r=$(echo "$1" | cut -f2 -d".")
    if [ "$r" -gt 0 ]; then
        v=$((v + 1))
    fi
    echo "$v"
}

function marker() {
    rank=$(awk "/${ISSN_val}/ {print NR}" "$matrix")

    if [ "$rank" -le "$(ceil "$(echo "0.25 * $n" | bc)")" ]; then
        classCNATDCU='ISI ROSU'

        if [[ "$container_title" == *"NATURE"* ]]; then
            classCNATDCU='NATURE'
        fi
    else
        if [ "$rank" -le "$(ceil "$(echo "0.5 * $n" | bc)")" ]; then
            if [ "$clasificCNATDCU" != "ISI ROSU" ]; then
                clasificCNATDCU='ISI GALBEN'
            fi
        else
            if [ "$clasificCNATDCU" != "ISI ROSU" ] && [ "$clasificCNATDCU" != "ISI GALBEN" ]; then
                clasificCNATDCU='ISI ALB'
            fi
        fi
    fi

    if [ "$INFO" == 'true' ]; then
        x="$(floor "$(echo "(0.2 * $(ceil "$(echo "0.25 * $n" | bc)"))" | bc)")"

        if [ "$rank" -le "$x" ]; then
            classINFO='A*'
        elif [ "$rank" -le "$(($(ceil "$(echo "0.25 * $n" | bc)") + x))" ]; then
            if [ "$classINFO" != 'A*' ]; then
                classINFO='A'
            fi
        else
            if [ "$rank" -le "$(($(ceil "$(echo "0.5 * $n" | bc)") + x))" ]; then
                if [ "$classINFO" != 'A*' ] && [ "$classINFO" != 'A' ]; then
                    classINFO='B'
                fi
            else
                if [ "$classINFO" != 'A*' ] && [ "$classINFO" != 'A' ] && [ "$classINFO" != 'B' ]; then
                    classINFO='C'
                fi
            fi
        fi
    fi
}

function similarity() {
    awk '
    function min(x, y) {
      return x < y ? x : y
    }
    function max(x, y) {
      return x > y ? x : y
    }
    function lev(s,t) {
      m = length(s)
      n = length(t)

      for(i=0;i<=m;i++) d[i,0] = i
      for(j=0;j<=n;j++) d[0,j] = j

      for(i=1;i<=m;i++) {
        for(j=1;j<=n;j++) {
          c = substr(s,i,1) != substr(t,j,1)
          d[i,j] = min(d[i-1,j]+1,min(d[i,j-1]+1,d[i-1,j-1]+c))
        }
      }

      return d[m,n]
    }

    BEGIN {
      print 100 - (100 * lev(ARGV[1], ARGV[2]) / max(length(ARGV[1]), length(ARGV[2])))
      exit
    }' "$1" "$2"
}

function float_calculator() {
    A="$1"
    OP="$2"
    B="$3"

    return "$(echo "$A $OP $B" | bc)"
}

function validate_doi_input() {
    if [[ "$1" == *"/"* ]]; then
        return 1
    fi
    return 0
}

function validate_bool_input() {
    if [ "$1" == "true" ] || [ "$1" == "false" ]; then
        return 1
    fi
    return 0
}

function help() {
    echo "Usage: $0 DOI INFO"
    echo "Where DOI in form of x/y and INFO is a boolean value"
}

########## MAIN VALIDATE INPUT ###########
if [ $# -ne 2 ]; then
    echo -e "Invalid number of arguments.\n"
    help
    exit 1
fi

DOI="$1"
INFO="$2"
if validate_doi_input "$DOI"; then
    echo -e "First argument is not a in form x/y.\n"
    help
    exit 1
fi

if validate_bool_input "$INFO"; then
    echo -e "Second argument is not a boolean value.\n"
    help
    exit 1
fi

################## MAIN ##################

isInWoS="false"
INPUT_DATA_DIR="./input-data"
TMP_DIR="$(mktemp -d)"
DOI_FILE="$TMP_DIR/output.json"

WOS=$(curl -s -I "http://ws.isiknowledge.com/cps/openurl/service?url_ver=Z39.88-2004&rft_id=info:doi/$DOI" | sed -n 's/.*WOS:\([0-9]\+\).*/\1/p')
if [ "$WOS" != "" ]; then
    isInWoS="true"
fi

curl -s -L -H 'Accept: application/citeproc+json, application/unixref+xml' "http://dx.DOI.org/$DOI" >"$DOI_FILE"
year="$(jq -r '.created."date-parts"[0][0]' "$DOI_FILE")"
years=("$((year - 1))" "$year" "$((year + 1))")
type="$(jq -r '.type' "$DOI_FILE")"

case $type in
'journal-article')
    readarray -t ISSN <<<"$(jq -r '.ISSN[]' "$DOI_FILE")"
    container_title="$(jq -r '."container-title"' "$DOI_FILE")"
    ;;
'proceedings-article')
    event="$(jq -r '."event"' "$DOI_FILE")"
    acronym="$(sed -n 's/.*(\([A-Z]\+\)).*/\1/p' <<<"$event")"
    event_title="$(sed -n 's/[0-9]\+ \(.*\) (\([a-zA-Z]\+\)).*/\1/p' <<<"$event")"
    ;;
'book-chapter' | 'book')
    publisher="$(jq -r '."publisher"' "$DOI_FILE" | cut -d' ' -f1)"
    publisher_location="$(jq -r '."publisher-location"' "$DOI_FILE" | cut -d, -f1)"
    ;;
*) ;;
esac

if [ "$type" == "journal-article" ]; then
    classCNATDCU=""
    classINFO="D"

    while IFS= read -d $'\0' -r JOURNAL; do
        JOURNAL_NAME="$(basename $JOURNAL | cut -d. -f1)"
        for ISSN_val in "${ISSN[@]}"; do
            search_file="$TMP_DIR/${JOURNAL_NAME}_${ISSN_val}.json"
            jq ".[] | select(.issn == \"$ISSN_val\")" "$JOURNAL" >"$search_file"

            if [ -s "$search_file" ]; then
                journal_impact_factor="$(jq -r '.journalImpactFactor' "$search_file")"
                article_influence_score="$(jq -r '.articleInfluenceScore' "$search_file")"
                category_name="$(jq -r '.categoryName' "$search_file")"

                similar_articles="$TMP_DIR/${JOURNAL_NAME}_${ISSN_val}_similar_articles.json"
                matrix="$TMP_DIR/${JOURNAL_NAME}_${ISSN_val}_matrix"
                jq ".[] | select(.categoryName == \"$category_name\")" "$JOURNAL" >"${similar_articles}"
                n="$(grep -c "$category_name" "$similar_articles")"

                if [ "$n" -gt 0 ]; then
                    jq -r '[.issn,.journalImpactFactor,.articleInfluenceScore]|@tsv' "${similar_articles}" | sort -rk2,2 >"$matrix"
                    # marker
                    marker
                    # end marker

                    if float_calculator "$article_influence_score" ">" "0"; then
                        sort -rk3,3 >"$matrix"
                        marker
                    fi
                fi
            else
                rm -f "${search_file}"
            fi
        done
    done < <(find "$INPUT_DATA_DIR/" \( -name '*SCIE*' -o -name '*SSCI*' \) \( -name "*${years[0]}*" -o -name "*${years[1]}*" -o -name "*${years[2]}*" \) -type f -print0)

    if [ "$classCNATDCU" == "" ] && [ "$isInWoS" ]; then
        classCNATDCU="ISI ESCI"
    fi
    if [ "$INFO" == "true" ] && [ "$classINFO" == 'D' ]; then
        if curl -Is "https://plu.mx/plum/a/?DOI=$DOI" | grep -qi scopus; then
            classINFO='C'
        fi
    fi
elif [ "$type" == "proceedings-article" ]; then
    x="$(echo "$DOI" | cut -d'/' -f1)"
    if [ "${isInWoS}" == "true" ]; then
        clasificCNATDCU='ISI PROC'
    elif [ "$x" == "10.1109" ]; then
        clasificCNATDCU='IEEE PROC'
    else
        clasificCNATDCU=''
    fi

    if [ "$INFO" == "true" ]; then
        classINFO='D'
        found=0

        if [ "$acronym" != '' ]; then
            while IFS= read -d $'\0' -r CORE_FILE; do
                if grep -q "$acronym" "$CORE_FILE"; then
                    found=1
                    break
                fi
            done < <(find "$INPUT_DATA_DIR/" -name "*CORE*" \( -name "*${years[0]}*" -o -name "*${years[1]}*" -o -name "*${years[2]}*" \) -type f -print0)

            if [ "$found" -eq 1 ]; then
                line="$(grep "$acronym" "$CORE_FILE")"
                match_event_title="$(awk '{for(i=2;i<=NF-5;i++){printf "%s ", $i}; printf "\n"}' <<<"$line")"

                similarity_percent="$(similarity "$event_title" "$match_event_title")"
                if float_calculator "$similarity_percent" ">=" "75"; then
                    classINFO="$(awk '{print $(NF-2)}' <<<"$line")"
                fi
            fi
        fi
    fi
fi
if [ "$INFO" == "true" ]; then
    if [ "$type" == "book-chapter" ] || [ "$type" == "book" ]; then
        found=0
        SENSE_FILE="$INPUT_DATA_DIR/SENSE.csv"
        line="$(grep "$publisher" "$SENSE_FILE" | grep "$publisher_location" | grep "$year" | head -n1)"
        RC=$?

        if [ $RC -eq 0 ]; then
            classINFO="$(awk -F, '{print $(NF)}' <<<"$line")"
        fi
    fi
fi

rm -rf "$TMP_DIR"

echo "DOI: $DOI"
echo "type: $type"
echo "clasificCNATDCU: $clasificCNATDCU"
if [ "$isInWoS" == "true" ]; then echo "isInWoS: $WOS"; fi
if [ "$INFO" == "true" ]; then echo "classINFO: $classINFO"; fi
